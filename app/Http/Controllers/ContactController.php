<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    private $create;
    public function __construct() {
            $this->create = [
                'name'    => 'required|string|max:70',
                'email'   => 'required|email',
                'message' =>'required|string|max:1024',
            ];
            $this->middleware('auth:api', ['except' => ['store']]);

        }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Contact::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->create);
        if ($validator->fails()) return response()->json($validator->errors(), 422);
        $data = $validator->validate();
        Contact::create($data);
        return response()->json(['responseText' => 'Success!'], 200);
    }
}
