<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    private $create;
        private $update;
        public function __construct() {
            $this->middleware('auth:api', ['except' => ['index', 'show']]);
        }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Product::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (auth('api')->user()->type != 'admin')
            return response()->json(['responseText' =>"Not authorized"], 401);
        $folderPath = "product_images/";
        $image_parts = explode(";base64,", $request->imageSource);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $file = $folderPath . uniqid() . '.png';
        Storage::disk('local')->put($file, $image_base64);
        $data = [
          'name' => $request->name,
          'description' => $request->description,
          'quantity' => $request->quantity,
          'amount' => $request->amount,
          'image' => json_encode([[
            'link' => $file,
            'filename' => explode('fakepath\\', $request->image)[1],
          ]])
        ];
        Product::create($data);
        return response()->json(['responseText' => 'Success!'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return $product;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
    }
}
