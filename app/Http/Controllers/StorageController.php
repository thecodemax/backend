<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StorageController extends Controller
{
    public function get(Request $request, $image){
        return response()->download(storage_path('app/product_images/' . $image));
    }
}
